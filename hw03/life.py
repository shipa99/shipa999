import pygame
from pygame.locals import *
import random


class GameOfLife:

    def __init__(self, width=640, height=480, cell_size=10, speed=10):
        self.width = width
        self.height = height
        self.cell_size = cell_size

        # Устанавливаем размер окна
        self.screen_size = width, height
        # Создание нового окна
        self.screen = pygame.display.set_mode(self.screen_size)

        # Вычисляем количество ячеек по вертикали и горизонтали
        self.cell_width = self.width // self.cell_size
        self.cell_height = self.height // self.cell_size

        # Скорость протекания игры
        self.speed = speed

    def draw_grid(self):
        """ Отрисовать сетку """
        for x in range(0, self.width, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('black'),
                             (x, 0), (x, self.height))
        for y in range(0, self.height, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('black'),
                             (0, y), (self.width, y))

    def run(self):
        """ Запустить игру """
        pygame.init()
        clock = pygame.time.Clock()
        pygame.display.set_caption('Game of Life')
        self.screen.fill(pygame.Color('white'))
        self.clist = self.cell_list()
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == QUIT:
                    running = False
            self.draw_grid()
            self.draw_cell_list(self.clist)
            self.clist = self.update_cell_list(self.clist)
            pygame.display.flip()
            clock.tick(self.speed)
        pygame.quit()

    def cell_list(self, randomize=True):
        """ Создание списка клеток.
        :param randomize: Если True, то создается список клеток, где
        каждая клетка равновероятно может быть живой (1) или мертвой (0).
        :return: Список клеток, представленный в виде матрицы
        """
        if randomize:
            self.clist = [[random.randint(0, 1) for x in range(self.cell_width)] for y in range(self.cell_height)]
        else:
            self.clist = [[0 for x in range(self.cell_width)] for y in range(self.cell_height)]
        return self.clist

    def draw_cell_list(self, clist):
        """ Отображение списка клеток
        :param rects: Список клеток для отрисовки, представленный в виде матрицы
        """
        x = y = 0
        for row in range(len(clist)):
            for each_cell in range(len(clist[row])):
                if clist[row][each_cell] == 1:
                    pygame.draw.rect(self.screen, pygame.Color('green'),
                                     (x + 1, y + 1, self.cell_size - 1, self.cell_size - 1))
                    x += self.cell_size
                else:
                    pygame.draw.rect(self.screen, pygame.Color('white'),
                                     (x + 1, y + 1, self.cell_size - 1, self.cell_size - 1))
                    x += self.cell_size
            x = 0
            y += self.cell_size

    def get_neighbours(self, cell):
        """ Вернуть список соседей для указанной ячейки
        :param cell: Позиция ячейки в сетке, задается кортежем вида (row, col)
        :return: Одномерный список ячеек, смежных к ячейке cell
        """
        neighbours = []
        row, col = cell
        offset = [[-1, 0], [1, 0], [0, -1], [0, 1], [-1, -1], [1, -1], [-1, 1], [1, 1]]
        for i in offset:
            if 0 <= row + i[0] < self.cell_height and 0 <= col + i[1] < self.cell_width:
                neighbours.append(self.clist[row + i[0]][col + i[1]])
        return neighbours

    def update_cell_list(self, cell_list):
        """ Выполнить один шаг игры.
        Обновление всех ячеек происходит одновременно. Функция возвращает
        новое игровое поле.
        :param cell_list: Игровое поле, представленное в виде матрицы
        :return: Обновленное игровое поле
        """
        new_clist = []
        y, x = 0, 0
        for i in range(len(cell_list)):
            new_clist.append([])
            for j in range(len(cell_list[i])):
                new_clist[i].append(0)
        for row in cell_list:
            for col in row:
                if col == 1:
                    if 1 < self.get_neighbours((y, x)).count(1) < 4:
                        new_clist[y][x] = 1
                    else:
                        new_clist[y][x] = 0
                else:
                    if self.get_neighbours((y, x)).count(1) == 3:
                        new_clist[y][x] = 1
                    else:
                        new_clist[y][x] = 0
                x += 1
            x = 0
            y += 1
        self.clist = new_clist
        return self.clist

if __name__ == '__main__':
    game = GameOfLife(400, 300, 5)
    game.run()
