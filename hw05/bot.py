import requests
import config
import telebot
import datetime
from bs4 import BeautifulSoup


bot = telebot.TeleBot(config.access_token)


def get_page(group, week=''):
    if week:
        week = str(week) + '/'
    url = '{domain}/{group}/{week}raspisanie_zanyatiy_{group}.htm'.format(domain=config.domain, week=week, group=group)
    response = requests.get(url)
    web_page = response.text
    return web_page


def parse_schedule_for_a_day(web_page, day_number):
    soup = BeautifulSoup(web_page, "html5lib")

    if day_number == '/monday':
        day_number = 1
    elif day_number == '/tuesday':
        day_number = 2
    elif day_number == '/wednesday':
        day_number = 3
    elif day_number == '/thursday':
        day_number = 4
    elif day_number == '/friday':
        day_number = 5
    elif day_number == '/saturday':
        day_number = 6
    elif day_number == '/sunday':
        day_number = 7

    # Получаем таблицу с расписанием на любой день недели
    schedule_table = soup.find("table", attrs={"id": str(day_number)+'day'})

    # Время проведения занятий
    if schedule_table != None:
        times_list = schedule_table.find_all("td", attrs={"class": "time"})
        times_list = [time.span.text for time in times_list]

        # Место проведения занятий
        locations_list = schedule_table.find_all("td", attrs={"class": "room"})
        locations_list = [room.span.text for room in locations_list]

        # Аудитория проведения занятий
        classrooms_list = schedule_table.find_all("dd", attrs={"class": "rasp_aud_mobile"})
        classrooms_list = [classroom.text for classroom in classrooms_list if classroom]

        # Название дисциплин и имена преподавателей
        lessons_list = schedule_table.find_all("td", attrs={"class": "lesson"})
        lessons_list = [lesson.text.split('\n\n') for lesson in lessons_list]
        lessons_list = [', '.join([info for info in lesson_info if info]) for lesson_info in lessons_list]

        return times_list, locations_list, classrooms_list, lessons_list
    else:
        return "Расписания на этот день нет"


@bot.message_handler(commands=['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'])
def get_schedule(message):
    """ Получить расписание на указанный день """
    day, week, group = message.text.split()
    web_page = get_page(group, week)
    if "Расписание не найдено" in web_page:
        pr = "Группа не найдена"
        bot.send_message(message.chat.id, pr, parse_mode='HTML')
return None

    if parse_schedule_for_a_day(web_page, day) != "Расписания на этот день нет":
        times_list, locations_list, classrooms_list, lessons_list = \
            parse_schedule_for_a_day(web_page, day)
        resp = ''
        for time, lesson, classroom, location in zip(times_list, lessons_list, classrooms_list, locations_list):
            resp += '<b>{}</b> {} {}, {}\n'.format(time, lesson, classroom, location)
        bot.send_message(message.chat.id, resp, parse_mode='HTML')
    else:
        resp = parse_schedule_for_a_day(web_page, day)
        bot.send_message(message.chat.id, resp, parse_mode='HTML')


@bot.message_handler(commands=['near'])
def get_near_lesson(message):
    """ Получить ближайшее занятие """
    _, group = message.text.split()
    cur_week = (datetime.date.today() - datetime.date(2017, 9, 1)) // 7
    if cur_week.days % 2 == 0:
        week = 1
    else:
        week = 2
    for i in range(1, 8):
        day = i
        web_page = get_page(group, week)
        if "Расписание не найдено" in web_page:
        pr = "Группа не найдена"
        bot.send_message(message.chat.id, pr, parse_mode='HTML')
return None
        times_list, locations_list, classrooms_list, lessons_list = parse_schedule_for_a_day(web_page, day)
        lesson_number = 0
        state = 0
        for j in times_list:
            _, time = j.split('-')
            t1, t2 = time.split(':')
            time = int(t1 + t2)
            cur_time = int(str(datetime.datetime.now().hour) + str(datetime.datetime.now().minute))
            if cur_time < time:
                resp = '<b>Ближайшее занятие:</b>\n'
                resp += '<b>{}</b>, {}, {}\n'.format(times_list[lesson_number], locations_list[lesson_number],
                                                     classrooms_list[lesson_number], lessons_list[lesson_number])
                bot.send_message(message.chat.id, resp, parse_mode='HTML')
                state = 1
                break
            lesson_number += 1

        if state != 1:
            today = datetime.date.today()
            if today.isoweekday == 6:
                next_day = today + datetime.timedelta(days=2)
            else:
                next_day = today + datetime.timedelta(days=1)
            cur_week = (next_day - datetime.date(2017, 9, 1)) // 7
            if (cur_week.days + 1) % 2 == 0:
                week = '1'
            else:
                week = '2'

            if next_day.isoweekday() == 1:
                next_day = '/monday'
            elif next_day.isoweekday() == 2:
                next_day = '/tuesday'
            elif next_day.isoweekday() == 3:
                next_day = '/wednesday'
            elif next_day.isoweekday() == 4:
                next_day = '/thursday'
            elif next_day.isoweekday() == 5:
                next_day = '/friday'
            elif next_day.isoweekday() == 6:
                next_day = '/saturday'

            times_list, locations_list, classrooms_list, lessons_list = parse_schedule_for_a_day(web_page, next_day)
            resp = '<b>Ближайшая пара:</b>\n'
            resp += '<b>{}</b>, {}, {}\n'.format(times_list[0], locations_list[0], classrooms_list[0], lessons_list[0])
            bot.send_message(message.chat.id, resp, parse_mode='HTML')
            break


@bot.message_handler(commands=['tommorow'])
def get_tommorow(message):
    """ Получить расписание на следующий день """
    tomorrow = datetime.datetime.now().isoweekday() + 1
    day = tomorrow
    week = ''
    _, group = message.text.split()
    web_page = get_page(group, week)
    if "Расписание не найдено" in web_page:
        pr = "Группа не найдена"
        bot.send_message(message.chat.id, pr, parse_mode='HTML')
return None
    if parse_schedule_for_a_day(web_page, day) != 'Расписания на этот день нет':
        times_list, locations_list, classrooms_list, lessons_list = \
            parse_schedule_for_a_day(web_page, day)
        resp = ''
        for time, lesson, location, classroom in zip(times_list, lessons_list, locations_list, classrooms_list):
            resp += '<b>{}</b> {} {}, {}\n'.format(time, lesson, location, classroom)
        bot.send_message(message.chat.id, resp, parse_mode='HTML')
    else:
        resp = parse_schedule_for_a_day(web_page, day)
        bot.send_message(message.chat.id, resp, parse_mode='HTML')


@bot.message_handler(commands=['all'])
def get_all_schedule(message):
    """ Получить расписание на всю неделю для указанной группы """
    _, week, group = message.text.split()
    web_page = get_page(group, week)
    if "Расписание не найдено" in web_page:
        pr = "Группа не найдена"
        bot.send_message(message.chat.id, pr, parse_mode='HTML')
return None
    for i in range(1, 8):
        day = i
        if parse_schedule_for_a_day(web_page, day) != "Расписания на этот день нет":
            times_list, locations_list, classrooms_list, lessons_list = \
                parse_schedule_for_a_day(web_page, day)
            resp = ''
            for time, lesson, classroom, location in zip(times_list, lessons_list, classrooms_list, locations_list):
                resp += '<b>{}</b> {} {}, {}\n'.format(time, lesson, classroom, location)
            bot.send_message(message.chat.id, resp, parse_mode='HTML')
        else:
            resp = parse_schedule_for_a_day(web_page, day)
            bot.send_message(message.chat.id, resp, parse_mode='HTML')


if __name__ == '__main__':
    bot.polling(none_stop=True)