def encrypt_caesar(plaintext):
    """
    Encrypts plaintext using a Caesar cipher.

    >>> encrypt_caesar("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar("python")
    'sbwkrq'
    >>> encrypt_caesar("Python3.6")
    'Sbwkrq3.6'
    >>> encrypt_caesar("")
    ''
    """
    ciphertext = ""
    for i in range(len(plaintext)):
        symb = plaintext[i]
        a = ord(symb)
        if 97 <= a <= 122 or 65 <= a <= 90:
            if 97 <= a + 3 <= 122 or 65 <= a + 3 <= 90:
                ciphertext += chr(a + 3)
            else:
                ciphertext += chr(a + 3 - 26)

        if 33 <= a <= 64 or 91 <= a <= 96 or 123 <= a <= 126:
            ciphertext += chr(a)
    return ciphertext


def decrypt_caesar(ciphertext):
    """
    Decrypts a ciphertext using a Caesar cipher.

    >>> decrypt_caesar("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar("sbwkrq")
    'python'
    >>> decrypt_caesar("Sbwkrq3.6")
    'Python3.6'
    >>> decrypt_caesar("")
    ''
    """
    plaintext = ""
    for i in range(len(ciphertext)):
        symb = ciphertext[i]
        a = ord(symb)
        if 97 <= a <= 122 or 65 <= a <= 90:
            if 97 <= a - 3 <= 122 or 65 <= a - 3 <= 90:
                plaintext += chr(a - 3)
            else:
                plaintext += chr(a - 3 + 26)

        if 33 <= a <= 64 or 91 <= a <= 96 or 123 <= a <= 126:
            plaintext += chr(a)
    return plaintext
