def encrypt_vigenere(plaintext, keyword):
    """
    >>> encrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> encrypt_vigenere("python3.6", "a")
    'python3.6'
    >>> encrypt_vigenere("ATTACKATDAWN", "LEMON")
    'LXFOPVEFRNHR'
    """
    ciphertext = ""
    kw_origin = keyword
    a = ord("a")
    A = ord("A")

    while len(keyword) < len(plaintext):
        keyword += kw_origin

    for i in range(len(plaintext)):
        ptcode = ord(plaintext[i])
        kwcode = ord(keyword[i])
        if 97 <= ptcode <= 122 and 97 <= kwcode <= 122:
            if 97 <= ptcode + (kwcode - a) <= 122:
                ciphertext += chr(ptcode + (kwcode - a))
            else:
                ciphertext += chr(ptcode + (kwcode - a) - 26)

        if 65 <= ptcode <= 90 and 65 <= kwcode <= 90:
            if 65 <= ptcode + (kwcode - A) <= 90:
                ciphertext += chr(ptcode + (kwcode - A))
            else:
                ciphertext += chr(ptcode + (kwcode - A) - 26)

        if 33 <= ptcode <= 64 or 91 <= ptcode <= 96 or 123 <= ptcode <= 126:
            ciphertext += chr(ptcode)
    return ciphertext


def decrypt_vigenere(ciphertext, keyword):
    """
    >>> decrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> decrypt_vigenere("python3.6", "a")
    'python3.6'
    >>> decrypt_vigenere("LXFOPVEFRNHR", "LEMON")
    'ATTACKATDAWN'
    """
    plaintext = ""
    kw_origin = keyword
    a = ord("a")
    A = ord("A")

    while len(keyword) < len(ciphertext):
        keyword += kw_origin

    for i in range(len(ciphertext)):
        ptcode = ord(ciphertext[i])
        kwcode = ord(keyword[i])
        if 97 <= ptcode <= 122 and 97 <= kwcode <= 122:
            if 97 <= ptcode - (kwcode - a) <= 122:
                plaintext += chr(ptcode - (kwcode - a))
            else:
                plaintext += chr(ptcode - (kwcode - a) + 26)

        if 65 <= ptcode <= 90 and 65 <= kwcode <= 90:
            if 65 <= ptcode - (kwcode - A) <= 90:
                plaintext += chr(ptcode - (kwcode - A))
            else:
                plaintext += chr(ptcode - (kwcode - A) + 26)

        if 33 <= ptcode <= 64 or 91 <= ptcode <= 96 or 123 <= ptcode <= 126:
            plaintext += chr(ptcode)
    return plaintext
